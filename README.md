# Utopia Models Repository Template

Working inside a clone or a fork of [the main Utopia repository][Utopia] is generally *not* a good idea: it makes updating harder, prohibits efficient version control on the models, and makes it more difficult to include additional dependencies or code.

For that reason, Utopia provides an easy way to create a separate project repository in which Utopia models can be implemented.
This page explains how to set up your own such repository.

*Note:* This template is for use with the *latest* version of Utopia.
If you require a template for Utopia v1, have a look at the corresponding [*release branch* `v1`](https://gitlab.com/utopia-project/models_template/-/tree/v1) of this project and make sure to use the `-c v1` option on the `cookiecutter` command.

[[_TOC_]]

## Instructions
To make setting up a project repository as easy as possible, we use [cookiecutter](https://github.com/cookiecutter/cookiecutter#features), which allows to clone a template project (this repository) and make the necessary adjustments on the way.
For instance, the `{{cookiecutter.project_slug}}` folder you see in the root of this repository will be named using the `project_slug` variable; the same holds for these kinds of variables *inside* any of the files in this project.


### Step 1: Install `cookiecutter`
```bash
pip3 install cookiecutter
```


### Step 2: Create your project from the template
Now, enter the directory in which your project repository should be created in.
If you are following our conventions from the documentation, this would be the `Utopia` directory into which you previously cloned the framework repository (i.e., *this* repository).

☝️**Before proceeding, read the rest of this step carefully!**

To create your project repository from the template, call the following command:

```bash
cookiecutter https://gitlab.com/utopia-project/models_template
```

This will prompt for some input from your side.
For some prompts (marked with 🌀), you probably want to keep the auto-generated default value (shown inside the square brackets) unless you *really* know what you're doing.

Let's go through the prompts one by one:

* `author`: your name or that of your team; may contain spaces
* `project_name`: the readable name of this project; may contain spaces
* `project_slug` 🌀 : used e.g. for the top-level directory name. Should not contain spaces.
* `project_slug_cmake` 🌀 : used for the CMake project name
* `project_description_short`: a one-sentence description of this project *(optional)*
* `version`: a version specifier for the CMake project
* `cxx_standard` 🌀 : which C++ standard to use. Cannot be below 17, because Utopia requires that already.
* `first_model_name`: the name of the dummy model that the project will be populated with.
    * This is a copy of the `CopyMeBare` model from the framework repository.
    * It is used to populate the new repository with some example code, otherwise the build system gets confused.
    * You can use it as a starting point for your new model; or delete it *after you have added another model (see below for more info).*
    * If you don't intend on using it, don't bother changing the name and just use the default.

Now follow the prompts and enter the corresponding information.
Refer to the [cookiecutter documentation][cookiecutter_docs] for more information on the available configuration options in this step.

After this, cookiecutter will hopefully inform you that it has succeeded.
Your models repository is now basically set up! 🎉


### Step 3: Test that it works
Enter the created directory; it will be named like the `project_slug` you entered above.
Follow the instructions in the *created project's* README file to configure it and build the base model:

```bash
mkdir build && cd build
cmake ..
make MyFirstModel
```

*Hint:* In case the Utopia framework is not found upon CMake configuration, you have two options:
- Specify the `-DUtopia_DIR=path/to/utopia` option in the command above
- *or*: Re-configure the Utopia *framework* with the `-DCMAKE_EXPORT_PACKAGE_REGISTRY=On` flag set.


### Step 4: Make adjustments *(optional)*
There are a bunch of adjustments you can now do on your repository.
These are *all* optional at this point, but some are recommended.

If you want to get into model-building already, skip this step.
You can always come back to it later.

For more detail and background information on the anatomy and integration of such a models project, refer to the [Utopia documentation on setting up a separate models repository][Utopia_docs_anatomy].

#### Set up version control *(recommended)*
Your new project does not come with version control that. We should definitely change that:

```bash
git init
git remote add origin <YOUR-PROJECT-URL>
```

You probably want a remote host for your git repository.
This template project is specialized for hosting on GitLab instances and provides issue and MR templates as well as a CI/CD configuration.
If you want to host there, create a new project now and follow the steps there to connect it to your local repository.

At this point, you can already commit and push if you want, but you can also wait and carry out any further adjustments; it's completely up to you.

For hosting on other platforms, the procedure is basically the same.
In such a case, you can delete the `.gitlab` directory and the `.gitlab-ci.yml`.


#### Adjust dependencies
The template models repository includes some often-used *optional* dependencies for convenience; see the newly-created README for more information.

You can either delete these or add new ones in the `cmake/modules/UtopiaModelsMacros.cmake` file.

*Hint:* We suggest to document these changes in the project README additionally, in the table and in the installation instructions; it's all set up for you so it should be easy to fill them in.


#### Populate GitLab CI/CD (or delete it)
*Proceed only, if you know what this is and have at least a little bit of experience with it.*

In case you want to use GitLab CI/CD for automated testing, the template repository comes with a `.gitlab-ci.yml` to get you started.
If you don't plan on using it, you can safely delete that file and the `docker` directory.

The CI/CD as configured in this template does the following:

* Build a testing "base" image using a Ubuntu-based [homebrew][homebrew_linux]; this image fulfils all Utopia dependencies.
* Update that testing image by installing the *additional* dependencies added by your project repository.
* Define a bunch of jobs:
    * The `build` stage builds the models and their C++ tests
    * The `test` stage carries out all the tests
    * The `report` stage provides information on the previous jobs, e.g. test code coverage.

In very broad strokes, *you* need to make the following adjustments manually:

* Check that the installation routine in the `docker/brewtopia-models.Dockerfile` matches that of your project repository.
  If you have added further dependencies, you will need to add them here as well.
* To build and deploy the testing image (and avoid re-building it each time), you will need some kind of container registry.
  Make an account on [Docker hub](https://hub.docker.com) and enter the credentials as secret variables (GitLab project settings -> CI/CD) for your new project.
    * `DOCKER_HUB_USER`: the user name
    * `DOCKER_HUB_PW`: the corresponding password. *Make this variable protected if you are not using the repository all by yourself.*


#### Delete the dummy model
⚠️ As stated above, the model created with `first_model_name` should *not* be deleted *unless* you have already added another model to the project repository.
The reason is that the build system is expecting at least one model in the repository and may do unexpected stuff otherwise (yes, this can be considered a bug).

*After* you have added one or more of your own models (e.g. in the next step), you can come back to this step.

To remove the dummy model, delete the directories of that name from:

* `python/model_plots`
* `python/model_tests`
* `src/models`
* `doc/models`

Additionally, delete the corresponding entries from:

* `add_subdirectory` line from `src/models/CMakeLists.txt`
* `test:model_defaults` job from `.gitlab-ci.yml`


### Step 5: Implement your own models
This is it, you have now got your own model project repository!

Follow the [Utopia documentation][Utopia_docs] for instructions on how to build your own model.


[Utopia]: https://gitlab.com/utopia-project/utopia
[Utopia_docs]: https://docs.utopia-project.org/
[Utopia_docs_anatomy]: https://docs.utopia-project.org/html/usage/implement/setup-models-repo.html
[cookiecutter_docs]: http://cookiecutter.readthedocs.io
[homebrew_linux]: https://docs.brew.sh/Homebrew-on-Linux
